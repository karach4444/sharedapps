//
//  BaseViewController.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        localize()
        // Do any additional setup after loading the view.
    }
    
    func setupView() { }
    
    func localize() { }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
