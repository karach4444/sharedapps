//
//  BaseRouter.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import UIKit

class BaseRouter {
    private(set) weak var hostViewController: UIViewController?
    
    init(hostViewController: UIViewController) {
        self.hostViewController = hostViewController
    }
}
