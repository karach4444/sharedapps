//
//  App.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import Foundation

struct App {
    enum ActionType {
        case chat, audioCall, videoCall
    }
    
    struct Capability {
        let urlBuilder: (String) -> URL
        let type: ActionType
    }
    let name: String
    let capabilities: [Capability]
}



// MARK: - Helpers
extension App {
    var canChat: Bool {
        return capabilities.contains(where: { $0.type == .chat })
    }
    var canAudioCall: Bool {
        return capabilities.contains(where: { $0.type == .audioCall })
    }
    var canVideoCall: Bool {
        return capabilities.contains(where: { $0.type == .videoCall })
    }
}
