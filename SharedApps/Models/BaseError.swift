//
//  BaseError.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 5.03.21.
//

import Foundation

struct BaseError: LocalizedError {
    private(set) var errorDescription: String?
    
    init(_ errorDescription: String) {
        self.errorDescription = errorDescription
    }
}
