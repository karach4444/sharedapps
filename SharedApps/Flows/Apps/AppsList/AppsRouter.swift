//
//  AppsRouter.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import UIKit

protocol AppsRouterProtocol {
    func showDetailsOf(app: App, openService: OpenAppsService)
}

class AppsRouter: BaseRouter, AppsRouterProtocol {
    func showDetailsOf(app: App, openService: OpenAppsService) {
        let appDetailsController = StoryboardScene.AppDetails.appDetailsController.instantiate()
        appDetailsController.presenter = AppDetailsPresenter(app: app, openService: openService)
        hostViewController?.navigationController?.pushViewController(appDetailsController, animated: true)
    }
}
