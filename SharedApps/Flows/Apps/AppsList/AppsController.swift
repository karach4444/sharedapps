//
//  ViewController.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import UIKit

protocol AppsViewProtocol: AnyObject {
    func update()
    func showAlertWith(error: Error)
}

class AppsController: BaseViewController {
    @IBOutlet private weak var tableView: UITableView!
    
    var presenter: AppsPresenterProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func setupView() {
        super.setupView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerCellNib(ofType: AppCell.self)
        tableView.rowHeight = UITableView.automaticDimension
        presenter.attachView(self)
    }
    
    override func localize() {
        super.localize()
        navigationItem.title = L10n.Apps.apps
    }
}

// MARK: - UITableViewDelegate

extension AppsController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didTapAppAt(index: indexPath.item)
    }
}

// MARK: - UITableViewDataSource

extension AppsController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        presenter.numberOfApps
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(ofType: AppCell.self, for: indexPath)
        cell.updateWith(state: presenter.getAppCellAt(index: indexPath.item))
        return cell
    }
}

// MARK: - AppsViewProtocol

extension AppsController: AppsViewProtocol {
    func showAlertWith(error: Error) {
        let alertController = UIAlertController(title: L10n.General.error, message: error.localizedDescription, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: L10n.General.ok, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func update() {
        tableView.reloadData()
    }
}
