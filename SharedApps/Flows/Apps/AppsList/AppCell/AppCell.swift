//
//  AppCell.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import UIKit

struct AppCellStateModel {
    let name: String
    
    init(app: App) {
        self.name = app.name
    }
}

class AppCell: UITableViewCell, NibLoadableView {
    @IBOutlet private weak var nameLabel: UILabel!
    
    func updateWith(state: AppCellStateModel) {
        nameLabel.text = state.name
    }
}
