//
//  AppsPresenter.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import Foundation
import UIKit

protocol AppsPresenterProtocol {
    var numberOfApps: Int { get }
    func attachView(_ view: AppsViewProtocol)
    func didTapAppAt(index: Int)
    func getAppCellAt(index: Int) -> AppCellStateModel
}

class AppsPresenter: AppsPresenterProtocol {
    private weak var view: AppsViewProtocol?
    private var router: AppsRouterProtocol
    private var apps: [App]
    private var openService: OpenAppsService
    
    var numberOfApps: Int {
        apps.count
    }
    
    init(apps: [App], openService: OpenAppsService, router: AppsRouterProtocol) {
        self.router = router
        self.apps = apps
        self.openService = openService
    }
    
    func attachView(_ view: AppsViewProtocol) {
        self.view = view
    }
    
    func didTapAppAt(index: Int) {
        if let url = apps[index].capabilities.first?.urlBuilder(""), openService.canOpen(url: url) {
            router.showDetailsOf(app: apps[index], openService: openService)
        } else {
            view?.showAlertWith(error: BaseError(L10n.Apps.noSuchAppError))
        }
    }
    
    func getAppCellAt(index: Int) -> AppCellStateModel {
        AppCellStateModel(app: apps[index])
    }
}

