//
//  AppDetailsPresenter.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import Foundation

protocol AppDetailsPresenterProtocol {
    func attachView(_ view: AppDetailsViewProtocol)
    func chatButtonDidTappedWith(phoneNumber: String)
    func callButtonDidTappedWith(phoneNumber: String)
    func videoCallButtonDidTappedWith(phoneNumber: String)
}

class AppDetailsPresenter: AppDetailsPresenterProtocol {
    private weak var view: AppDetailsViewProtocol?
    private let app: App
    private let openService: OpenAppsService
    
    init(app: App, openService: OpenAppsService) {
        self.app = app
        self.openService = openService
    }
    
    func attachView(_ view: AppDetailsViewProtocol) {
        self.view = view
        view.updateWith(app: app)
    }
    
    func chatButtonDidTappedWith(phoneNumber: String) {
        if let url = app.capabilities.first(where: { $0.type == .chat })?.urlBuilder(phoneNumber) {
            do {
               try openService.openWith(url: url)
            } catch let error {
                view?.showAlertWith(error: error)
            }
        }
    }
    
    func callButtonDidTappedWith(phoneNumber: String) {
        if let url = app.capabilities.first(where: { $0.type == .audioCall })?.urlBuilder(phoneNumber) {
            do {
               try openService.openWith(url: url)
            } catch let error {
                view?.showAlertWith(error: error)
            }
        }
    }
    
    func videoCallButtonDidTappedWith(phoneNumber: String) {
        if let url = app.capabilities.first(where: { $0.type == .videoCall })?.urlBuilder(phoneNumber) {
            do {
               try openService.openWith(url: url)
            } catch let error {
                view?.showAlertWith(error: error)
            }
        }
    }
}
