//
//  AppDetailsController.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import UIKit

protocol AppDetailsViewProtocol: AnyObject {
    func updateWith(app: App)
    func showAlertWith(error: Error)
}

class AppDetailsController: BaseViewController {
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var phoneLabel: UILabel!
    @IBOutlet private weak var phoneTextField: UITextField!
    @IBOutlet private weak var chatButton: UIButton!
    @IBOutlet private weak var callButton: UIButton!
    @IBOutlet private weak var videoCallButton: UIButton!
    
    var presenter: AppDetailsPresenterProtocol!
    
    @IBAction private func chatButtonDidTapped(_ sender: UIButton) {
        guard let phone = phoneTextField.text else { return }
        presenter.chatButtonDidTappedWith(phoneNumber: phone)
    }
    
    @IBAction private func callButtonDidTapped(_ sender: UIButton) {
        guard let phone = phoneTextField.text else { return }
        presenter.callButtonDidTappedWith(phoneNumber: phone)
    }
    
    @IBAction private func videoCallButtonDidTapped(_ sender: UIButton) {
        guard let phone = phoneTextField.text else { return }
        presenter.videoCallButtonDidTappedWith(phoneNumber: phone)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.attachView(self)
    }
    
    override func localize() {
        super.localize()
        phoneLabel.text = L10n.AppDetails.identificator
        chatButton.setTitle(L10n.AppDetails.chat, for: .normal)
        callButton.setTitle(L10n.AppDetails.call, for: .normal)
        videoCallButton.setTitle(L10n.AppDetails.videoCall, for: .normal)
    }
}

// MARK: - AppDetailsViewProtocol

extension AppDetailsController: AppDetailsViewProtocol {
    func updateWith(app: App) {
        navigationItem.title = app.name
        nameLabel.text = app.name
        if !app.canChat {
            chatButton.isHidden = true
        }
        if !app.canAudioCall {
            callButton.isHidden = true
        }
        if !app.canVideoCall {
            videoCallButton.isHidden = true
        }
    }
    
    func showAlertWith(error: Error) {
        let alertController = UIAlertController(title: L10n.General.error, message: error.localizedDescription, preferredStyle: .alert)
        let cancelAction = UIAlertAction(title: L10n.General.ok, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
}
