//
//  AppFlowRouter.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import UIKit

protocol AppFlowRouterProtocol {
    func openAppsListScreen(apps: [App])
}

class AppFlowRouter: AppFlowRouterProtocol {
    private var window: UIWindow
    private let openAppsService: OpenAppsService
    
    
    init(window: UIWindow, service: OpenAppsService) {
        self.window = window
        self.openAppsService = service
    }
    
    private func createAppsFlow(apps: [App]) -> UIViewController {
        let appsController = StoryboardScene.Apps.appsController.instantiate()
        appsController.presenter = AppsPresenter(apps: apps, openService: openAppsService, router: AppsRouter(hostViewController: appsController))
        let navigationController = BaseNavigationController(rootViewController: appsController)
        return navigationController
    }
    
    func openAppsListScreen(apps: [App]) {
        window.rootViewController = createAppsFlow(apps: apps)
    }
}
