//
//  AppFlowManager.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 4.03.21.
//

import Foundation

class AppFlowManager {
    private let apps: [App] = [
        App(name: "Messages", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "sms:\(identificator)")!}, type: .chat)]),
        App(name: "FaceTime", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "facetime:\(identificator)")! }, type: .videoCall), App.Capability(urlBuilder: { identificator in URL(string: "facetime-audio:\(identificator)")!}, type: .audioCall)]),
        App(name: "WhatsApp", capabilities: [App.Capability(urlBuilder: { _ in URL(string: "whatsapp:")!}, type: .chat)]),
        App(name: "Telephone", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "tel:\(identificator)")!}, type: .audioCall)]),
        App(name: "Facebook", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "fb-messenger://user-thread/\(identificator)")!}, type: .chat)]),
        App(name: "WeChat", capabilities: [App.Capability(urlBuilder: {_ in URL(string: "weixin:")!}, type: .chat)]), //Doesnt have api for chatting
        App(name: "Viber", capabilities: [App.Capability(urlBuilder: { _ in URL(string: "viber://chats")!}, type: .chat), App.Capability(urlBuilder: { _ in URL(string: "viber://calls")!}, type: .audioCall)]),
        App(name: "Telegram", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "tg://msg?text=\(identificator)")!}, type: .chat)]),
        App(name: "Skype", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "skype:\(identificator)?chat")!}, type: .chat), App.Capability(urlBuilder: { identificator in URL(string: "skype:\(identificator)?call")!}, type: .audioCall), App.Capability(urlBuilder: { identificator in URL(string: "skype:\(identificator)?call&video=true")!}, type: .videoCall)]),
        App(name: "Line", capabilities: [App.Capability(urlBuilder: {_ in URL(string: "line:")!}, type: .chat)]), //LINE URL scheme of 'line://' is deprecated
        App(name: "SnapChat", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "snapchat://add/\(identificator)")!}, type: .chat)]), // can only add user by username
        App(name: "Signal", capabilities: [App.Capability(urlBuilder: {_ in URL(string: "sgnl:")!}, type: .chat)]), //Signal does not have a URL scheme
        App(name: "Mail", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "mailto:\(identificator)")!}, type: .chat)]),
        App(name: "Hangouts", capabilities: [App.Capability(urlBuilder: { identificator in URL(string: "com.google.hangouts://textchat?p=g\(identificator)")!}, type: .chat)])
    ]
    var router: AppFlowRouterProtocol
    
    init(router: AppFlowRouterProtocol) {
        self.router = router
    }
    
    func openAppsListPage() {
        router.openAppsListScreen(apps: apps)
    }
}
