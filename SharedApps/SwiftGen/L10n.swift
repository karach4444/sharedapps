// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name
internal enum L10n {

  internal enum AppDetails {
    /// App Details
    internal static let appDetails = L10n.tr("Localizable", "AppDetails.appDetails")
    /// Call
    internal static let call = L10n.tr("Localizable", "AppDetails.call")
    /// Chat
    internal static let chat = L10n.tr("Localizable", "AppDetails.chat")
    /// Identificator
    internal static let identificator = L10n.tr("Localizable", "AppDetails.identificator")
    /// Video Call
    internal static let videoCall = L10n.tr("Localizable", "AppDetails.videoCall")
  }

  internal enum Apps {
    /// Apps
    internal static let apps = L10n.tr("Localizable", "Apps.apps")
    /// There is no such app on your iphone
    internal static let noSuchAppError = L10n.tr("Localizable", "Apps.noSuchAppError")
  }

  internal enum General {
    /// Error
    internal static let error = L10n.tr("Localizable", "General.error")
    /// Ok
    internal static let ok = L10n.tr("Localizable", "General.ok")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: nil, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    Bundle(for: BundleToken.self)
  }()
}
// swiftlint:enable convenience_type
