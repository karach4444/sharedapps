//
//  openAppsService.swift
//  SharedApps
//
//  Created by Anton Karachinskiy on 5.03.21.
//

import UIKit

class OpenAppsService {
    func canOpen(url: URL) -> Bool {
        UIApplication.shared.canOpenURL(url)
    }
    
    func openWith(url: URL) throws {
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        } else {
            throw BaseError("Can't find application")
        }
    }
}
